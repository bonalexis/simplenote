import { Main, MessageNoNote, Side, Ul, AddNote, Search, SwitchTheme } from "./App.style";
import { lightTheme, darkTheme, GlobalStyle } from "./GlobalStyle";
import { Note } from "./components/Note";
import { ThemeProvider } from "styled-components";
import { useEffect } from "react";
import { useState } from "react";
import LinkToNote from "./components/LinkToNote";
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import { Loading } from "./components/Note.style";
import { BiMoon, BiSun } from "react-icons/bi";

const App = () => {
  const [notes, setNotes] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [activeNote, setActiveNote] = useState(null);
  const [theme, setTheme] = useState(darkTheme);

  const navigate = useNavigate();

  const fetchNotes = async () => {
    const notes = await fetch('/notes?_sort=date&_order=desc')
      .then(res => res.json());
    setIsLoading(false);
    setNotes(notes);
  }
  
  const searchNotes = async (query) => {
    const notes = await fetch(`/notes?q=${query}&_sort=date&_order=desc`)
      .then(res => res.json());

    setIsLoading(false);
    setNotes(notes);
  }

  useEffect(() => {
    fetchNotes();
  }, []);

  const updateNote = (newNote) => {
    setNotes(notes.map((note) => note.id === newNote.id ? newNote : note).sort((a, b) => b.date - a.date));    
  }

  const addNote = () => {
    fetch('/notes', {
      method: "POST",
      body: JSON.stringify({
        titre: "Nouvelle note",
        date: Date.now()
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(json => {
        setNotes(notes.concat([json]).sort((a, b) => b.date - a.date));
        navigate(`/notes/${json.id}`);
      })
  }

  const rmNote = (id) => {
    fetch(`/notes/${id}`, { method: "DELETE" })
      .then(() => {
        setNotes(notes.filter((note) => note.id !== parseInt(id)));
      });
  }

  const toggleTheme = () => {
    setTheme(theme === darkTheme ? lightTheme : darkTheme);
  }


  return (
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Side>
          <SwitchTheme onClick={toggleTheme}>{theme === darkTheme ? <BiSun /> : <BiMoon />}</SwitchTheme>

          <Search placeholder="Rechercher..." onInput={(event) => {
            searchNotes(event.target.value);
          }}></Search>
          <Ul>
            {notes
              ? notes.map((note) => {
                return (
                  <li key={note.id}>
                    <LinkToNote active={parseInt(activeNote) === note.id} id={note.id} title={note.titre} contenu={note.contenu} />
                  </li>
                );
              })
              : isLoading
                ? <Loading />
                : "Aucune note..."
            }
          </Ul>
          <AddNote onClick={addNote}>Ajouter</AddNote>
        </Side>
        <Main>
          <Routes>
            <Route path="/" element={<MessageNoNote>{!isLoading && "Sélectionner une note"}</MessageNoNote>} />
            <Route path="/notes/:id" element={<Note onModif={updateNote} onRemove={rmNote} setActive={setActiveNote} />} />
          </Routes>
        </Main>
      </ThemeProvider>
  );
}

export default App;
