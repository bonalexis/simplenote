import { Button, ButtonMenu, Form, Loading, Options, Option, SaveAndStatus, Saver, TextArea, Titre } from "./Note.style";
import { useNavigate, useParams } from "react-router-dom";
import { useState, useEffect, useCallback } from "react";
import { BiCheck, BiError, BiFile, BiSave, BiTrash } from "react-icons/bi";
import ReactMarkdown from 'react-markdown'

export const Note = ({ onModif, onRemove, setActive }) => {
    const { id } = useParams();

    const [note, setNote] = useState(null);
    const [iconSave, setIconSave] = useState(<BiCheck />);
    const [menu, setMenu] = useState(false);
    const [markdown, setMarkdown] = useState(false);

    const navigate = useNavigate();

    const fetchNote = useCallback(async () => {
        setNote(null)
        const note = await fetch(`/notes/${id}`)
            .then(res => res.json());

        setNote(note);
    }, [id])

    useEffect(() => {
        fetchNote();
        setActive(id);
    }, [id, fetchNote]);

    const saveNote = async (note) => {
        setIconSave(<Loading />);
        setNote(note);

        const response = await fetch(`/notes/${id}`, {
            method: 'PUT',
            body: JSON.stringify(note),
            headers: {
                "Content-Type": "application/json"
            }
        });

        if (response.ok) {
            onModif(note);
            setIconSave(<BiCheck />);
        } else {
            setIconSave(<BiError />);
        }
    }

    const submitForm = (event) => {
        event.preventDefault();
        saveNote(note);
    }

    const toggleMenu = () => {
        setMenu(!menu);
    }

    const toggleMarkdown = () => {
        setMarkdown(!markdown);
    }

    return (
        <>
            <Form onSubmit={submitForm}>
                {note
                    ? (<>
                        <Titre type="text" value={note ? note.titre : ""} onInput={(event) => {
                            saveNote({ ...note, titre: event.target.value, date: Date.now() });
                        }} />

                        {markdown
                            ? <TextArea value={note ? note.contenu : ""} onInput={(event) => {
                                saveNote({ ...note, contenu: event.target.value, date: Date.now() });
                            }} />
                            : <ReactMarkdown>{note ? note.contenu : ""}</ReactMarkdown>}
                        {
                            markdown && <Saver>
                                <Button>Enregistrer</Button>
                                <SaveAndStatus>{iconSave}</SaveAndStatus>

                            </Saver>

                        }
                    </>)
                    : <Loading />
                }
            </Form>
            {note
                &&
                <Options>
                    <ButtonMenu onClick={toggleMenu} />
                    {
                        menu &&
                        <>
                            <Option onClick={() => { toggleMarkdown(); toggleMenu() }}><BiFile /> Markdown</Option>
                            <Option onClick={() => {
                                navigate('/');
                                onRemove(id);
                            }}><BiTrash /> Supprimer</Option>
                        </>
                    }
                </Options>}
        </>
    )
}