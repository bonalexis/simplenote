import { Title, Contenu, Wrapper } from './LinkToNote.style';

const LinkToNote = ({ id, title, contenu, active }) => {
    return (
        <Wrapper to={`/notes/${id}`} className={active && "active"}>
            <Title>{title}</Title>
            <Contenu>{contenu}</Contenu>
        </Wrapper>
    )
}

export default LinkToNote;