import styled from "styled-components";
import {Link} from "react-router-dom";

export const Title = styled.h2`
    font-size: 1.3em;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    margin: 0;
`;

export const Contenu = styled.p`
    font-size: 0.8em;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    margin: 0;
`;

export const Wrapper = styled(Link)`
    padding: 16px;
    text-decoration: none;
    color: inherit;
    display: flex;
    flex-direction: column;

    :hover {
        background-color: ${(props) => props.theme.mainBackgroundColor};
    }

    &.active {
        background-color: ${(props) => props.theme.mainBackgroundColor};
    }
`;
