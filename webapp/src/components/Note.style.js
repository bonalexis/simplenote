import styled from "styled-components";
import { BiLoader, BiMenu } from "react-icons/bi";

export const Form = styled.form`
  height: 100%;

  display: flex;
  flex-direction: column;
  gap: 8px;
`;

export const Titre = styled.input`
  justify-items: stretch;
  padding: 8px;
  font-size: 3em;
  font-weight: 700;
  background-color: transparent;
  color: ${(props) => props.theme.textColor};
  border: none;
`;

export const TextArea = styled.textarea`
  flex: 1;
  padding: 8px;
  justify-items: stretch;
  overflow: scroll;
  resize: none;
  background-color: transparent;
  color: ${(props) => props.theme.textColor};
  border: none;
`;



export const Saver = styled.div`
  display: flex;
  justify-content: center;
`;

export const SaveAndStatus = styled.div`
  height: auto;
  padding: 8px;
  background-color: #028090;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Loading = styled(BiLoader)`
  -webkit-animation: icon-spin 2s infinite linear;
          animation: icon-spin 2s infinite linear;

  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

@-webkit-keyframes icon-spin {
  0% {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
            transform: rotate(359deg);
  }
}

@keyframes icon-spin {
  0% {
    -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
  }
  100% {
    -webkit-transform: rotate(359deg);
            transform: rotate(359deg);
  }
}
`

export const Button = styled.button`
  background-color: #76c893;
  color: #0D1B2A;
  border: none;
  font-weight: 600;
  font-size: 1.2em;
  padding: 8px;

  :hover {
    background-color: #52b69a;
    cursor: pointer;
  }
`;

export const Options = styled.div`
  position: fixed;
  top: 0;
  right: 0;

  display: flex;
  flex-direction: column;
  align-items: flex-end;

`;

export const ButtonMenu = styled(BiMenu)`
  width: 32px;
  height: 32px;

  :hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;
export const Option = styled.button`
  position: relative;
  top: 0;
  padding: 16px;
  font-size: 1em;
  width: 100%;
  background-color: inherit;
  color: ${(props) => props.theme.textColor};
  border: none;
  background-color: ${(props) => props.theme.asideBackgroundColor};

  :hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;