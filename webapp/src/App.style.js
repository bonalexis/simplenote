import styled from "styled-components";

export const Side = styled.aside`
  position: fixed;
  width: 240px;
  display: flex;
  flex-direction: column;
  top: 0;
  left: 0;
  bottom: 0;
  background-color: ${(props) => props.theme.asideBackgroundColor};
`;

export const Main = styled.main`
  padding: 16px;
  background-color: ${(props) => props.theme.mainBackgroundColor};
  margin-inline-start: 240px;
  height: 100vh;
`;

export const Ul = styled.ul`
  list-style: none;
  height: 100%;
  width: 100%;
  padding: 0;
  margin: 0;
  overflow: hidden scroll;
`;

export const SwitchTheme = styled.button`
  background-color: ${(props) => props.theme.mainBackgroundColor};
  color: ${(props) => props.theme.textColor};
  border: none;
  padding: 8px;
  margin: 8px;

  :hover {
   opacity: 0.8;
  }
`;

export const Search = styled.input`
  background-color: ${(props) => props.theme.mainBackgroundColor};
  border: none;
  padding: 8px;
  margin: 8px;
  color: ${(props) => props.theme.textColor};

  :hover {
   opacity: 0.8;
  }
`

export const MessageNoNote = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 2em;
  font-weight: 700;
`;


export const AddNote = styled.button`
  background-color: #76c893;
  color: #0D1B2A;
  border: none;
  font-weight: 600;
  font-size: 1.2em;
  padding: 8px;

  :hover {
    background-color: #52b69a;
    cursor: pointer;
  }
`;