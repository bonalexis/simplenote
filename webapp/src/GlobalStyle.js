import { createGlobalStyle, ThemeProvider } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box;
    }

    *:focus {
        outline: none;
        box-shadow: inset 0 0 0 1px lightblue;
    }

    body {
        margin: 0;
        color: ${(props) => props.theme.textColor};
        background-color: ${(props) => props.theme.mainBackgroundColor};
        font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif;
    }
`;

export const darkTheme = {
    mainBackgroundColor: '#415A77',
    asideBackgroundColor: '#0d1b2a',
    textColor: '#E0E1DD'
}

export const lightTheme = {
    mainBackgroundColor: '#f2e9e4',
    asideBackgroundColor: '#c9ada7',
    textColor: '#22223b'
}