# API json-server

## Lancement du serveur

Le serveur utilise le fichier `db.json` qu'il faut initialiser dans le répertoire `api` avec le contenu suivant :

```json
{
    "notes": [
        {
            "id": 1,
            "titre": "Initiation à REACT",
            "contenu": "Lorem ipsum..."
        },
        {
            "id": 2,
            "titre": "Utilisation de json-server",
            "contenu": "Bla Bla!"
        }
    ],
    "profile": {
        "name": "alexis"
    }
}
```

La commande permettant de lancer ce serveur est la suivante :

```bash
npm run startdb
```
